---@class MODIFIER_TABLE
---@field public caster CDOTA_BaseNPC
---@field public target CDOTA_BaseNPC
---@field public ability CDOTA_Ability_Lua
---@field public modifier_name string
---@field public duration number

---@class STACKING_MODIFIER_TABLE
---@field public caster CDOTA_BaseNPC
---@field public target CDOTA_BaseNPC
---@field public ability CDOTA_Ability_Lua
---@field public modifier_name string
---@field public duration number
---@field public stacks number
---@field public max_stacks number


---@param args MODIFIER_TABLE
---@return CDOTA_Modifier_Lua
function GameMode:ApplyBuff(args)
    if (args ~= nil) then
        args.duration = tonumber(args.duration)
        if (args.caster ~= nil and args.target ~= nil and args.modifier_name ~= nil and args.duration ~= nil) then
            return args.target:AddNewModifier(args.caster, args.ability, args.modifier_name, { Duration = args.duration })
        end
    end
    return nil
end

---@param args STACKING_MODIFIER_TABLE
---@return CDOTA_Modifier_Lua
function GameMode:ApplyStackingBuff(args)
    if (args ~= nil and args.stacks ~= nil and args.max_stacks ~= nil and args.stacks > 0 and args.max_stacks > 0) then
        local modifier = GameMode:ApplyBuff(args)
        if (modifier ~= nil) then
            local stacks = args.target:GetModifierStackCount(args.modifier_name, args.target) + args.stacks
            args.target:SetModifierStackCount(args.modifier_name, args.caster, math.min(stacks, args.max_stacks))
            return modifier
        end
    end
    return nil
end

---@param args MODIFIER_TABLE
---@return CDOTA_Modifier_Lua
function GameMode:ApplyDebuff(args)
    if (args ~= nil) then
        args.duration = tonumber(args.duration)
        if (args.caster ~= nil and args.target ~= nil and args.modifier_name ~= nil and args.duration ~= nil) then
            return args.target:AddNewModifier(args.caster, args.ability, args.modifier_name, { Duration = args.duration })
        end
    end
    return nil
end

---@param args STACKING_MODIFIER_TABLE
---@return CDOTA_Modifier_Lua
function GameMode:ApplyStackingDebuff(args)
    if (args ~= nil and args.stacks ~= nil and args.max_stacks ~= nil and args.stacks > 0 and args.max_stacks > 0) then
        local modifier = GameMode:ApplyDebuff(args)
        if (modifier ~= nil) then
            local stacks = args.target:GetModifierStackCount(args.modifier_name, args.target) + args.stacks
            args.target:SetModifierStackCount(args.modifier_name, args.caster, math.min(stacks, args.max_stacks))
            return modifier
        end
    end
    return nil
end

---@class REDUCE_ABILITY_CD_TABLE
---@field public target CDOTA_BaseNPC
---@field public ability CDOTA_Ability_Lua
---@field public reduction number
---@field public isflat boolean

---@param args REDUCE_ABILITY_CD_TABLE
function GameMode:ReduceAbilityCooldown(args)
    local target = args.target
    local ability = args.ability
    local reduction = args.reduction or 0
    local IsFlat = args.isflat or true
    if (target ~= nil and ability ~= nil) then
        ability = target:FindAbilityByName(ability)
        if (ability ~= nil) then
            local abilityLevel = ability:GetLevel()
            if (abilityLevel > 0) then
                local abilityCooldown = ability:GetCooldown(abilityLevel)
                local minCooldown = abilityCooldown * 0.5
                local reducedCooldown = abilityCooldown
                if (IsFlat) then
                    reducedCooldown = reducedCooldown - reduction
                else
                    reducedCooldown = reducedCooldown * reduction
                end
                if (reducedCooldown < minCooldown) then
                    reducedCooldown = minCooldown
                end
                ability:EndCooldown()
                ability:StartCooldown(reducedCooldown)
            end
        end
    end
end

---@class DAMAGE_TABLE
---@field public caster CDOTA_BaseNPC
---@field public target CDOTA_BaseNPC
---@field public damage number
---@field public ability CDOTA_Ability_Lua
---@field public physdmg boolean
---@field public puredmg boolean
---@field public firedmg boolean
---@field public frostdmg boolean
---@field public earthdmg boolean
---@field public naturedmg boolean
---@field public voiddmg boolean
---@field public infernodmg boolean
---@field public holydmg boolean
---@field public fromsummon boolean

---@param args DAMAGE_TABLE
function GameMode:DamageUnit(args)
    if (args == nil) then
        return
    end
    local caster = args.caster
    local target = args.target
    local damage = tonumber(args.damage)
    if (caster == nil or target == nil or damage == nil) then
        return
    end
    local ability = args.ability
    local damageTable = {
        victim = target,
        attacker = caster,
        damage = damage,
        damage_type = DAMAGE_TYPE_PURE,
        ability = ability
    }
    ApplyDamage(damageTable)
end

---@param unit CDOTA_BaseNPC
function GameMode:GetUnitAttackSpeed(unit)
    if (unit ~= nil) then
        return math.floor((unit:GetAttackSpeed() * 100) - 100)
    end
    return 0
end

