if Heroes == nil then
    _G.Heroes = class({})
end

---@class HERO_STATS_ELEMENTS_TABLE
---@field public fire number
---@field public frost number
---@field public earth number
---@field public void number
---@field public holy number
---@field public nature number
---@field public inferno number

---@class HERO_STATS_TABLE
---@field public str number
---@field public agi number
---@field public int number
---@field public damageReduction number
---@field public spellDamage number
---@field public spellHaste number
---@field public armor number
---@field public block number
---@field public magicBlock number
---@field public movespeedBonus number
---@field public castRangeBonus number
---@field public attackRangeBonus number
---@field public attackSpeed number
---@field public hpRegen number
---@field public mpRegen number
---@field public currentMana number
---@field public maxMana number
---@field public elementsProtection HERO_STATS_ELEMENTS_TABLE
---@field public elementsDamage HERO_STATS_ELEMENTS_TABLE

function Heroes:Init()
    Heroes.STATS_CALCULATE_INTERVAL = 1
end

---@param hero CDOTA_BaseNPC_Hero
function Heroes:OnHeroCreation(hero)
    if (hero ~= nil and IsServer()) then
        -- changing that require changes in party.js (MAX_LIVES)
        hero.currentLives = 5
        hero:AddNewModifier(hero, nil, "modifier_hero_system", { Duration = -1 })
        hero:AddNewModifier(hero, nil, "modifier_hero_lives", { Duration = -1 })
        hero:SetModifierStackCount("modifier_hero_lives", hero, hero.currentLives)
        TalentTree:SetupForHero(hero)
        Inventory:SetupForHero(hero)
        Summons:SetupForHero(hero)
    end
end

---@param hero CDOTA_BaseNPC_Hero
---@param statsTable HERO_STATS_TABLE
---@return HERO_STATS_TABLE
function Heroes:CalculateStats(hero, statsTable)
    if (hero ~= nil and statsTable ~= nil and IsServer()) then
        local heroLevel = hero:GetLevel()
        -- str, agi, int
        local heroBaseStr = math.floor(hero:GetBaseStrength() + (heroLevel - 1) * hero:GetStrengthGain())
        local heroBaseAgi = math.floor(hero:GetBaseAgility() + (heroLevel - 1) * hero:GetAgilityGain())
        local heroBaseInt = math.floor(hero:GetBaseIntellect() + (heroLevel - 1) * hero:GetIntellectGain())
        statsTable.str = heroBaseStr
        statsTable.agi = heroBaseAgi
        statsTable.int = heroBaseInt
        local switchTable = { statsTable.str, statsTable.agi, statsTable.int }
        local primaryAttribute = switchTable[hero:GetPrimaryAttribute()]
        -- attack damage
        local heroBaseDamage = 40
        local attackDamagePerPrimary = 1
        local totalAttackDamage = heroBaseDamage + (primaryAttribute * attackDamagePerPrimary)
        hero:SetBaseDamageMax(totalAttackDamage)
        hero:SetBaseDamageMin(totalAttackDamage)
        -- attack speed
        local heroBaseAttackSpeed = 0
        local attackSpeedPerAgi = 1
        local totalAttackSpeed = heroBaseAttackSpeed + (attackSpeedPerAgi * statsTable.agi)
        statsTable.attackSpeed = totalAttackSpeed
        hero:SetModifierStackCount("modifier_hero_system_aaspeed", hero, statsTable.attackSpeed)
        -- spell damage
        local baseSpellDamage = 0
        statsTable.spellDamage = baseSpellDamage
        -- spell haste
        local baseSpellHaste = 0
        statsTable.spellHaste = baseSpellHaste
        -- attack range
        statsTable.attackRangeBonus = 0
        hero:SetModifierStackCount("modifier_hero_system_aarange", hero, statsTable.attackRangeBonus)
        -- cast range
        statsTable.castRangeBonus = 0
        hero:SetModifierStackCount("modifier_hero_system_castrange", hero, statsTable.castRangeBonus)
        -- move speed
        statsTable.movespeedBonus = 0
        hero:SetModifierStackCount("modifier_hero_system_movespeed", hero, statsTable.movespeedBonus)
        -- hp regen
        local healthRegenPerStr = 0.1
        local baseHpRegen = (statsTable.str * healthRegenPerStr)
        local totalHpRegen = baseHpRegen
        statsTable.hpRegen = totalHpRegen
        -- mana regen
        local manaRegenPerInt = 0.05
        local baseMpRegen = (statsTable.int * manaRegenPerInt)
        local totalMpRegen = baseMpRegen
        statsTable.mpRegen = totalMpRegen
        -- max hp
        local healthPerStr = 20
        local baseHealthBonus = (statsTable.str * healthPerStr)
        local totalHealthBonus = baseHealthBonus
        statsTable.bonusHealth = totalHealthBonus
        -- max mp
        local manaPerInt = 12
        local baseManaBonus = (manaPerInt * statsTable.int)
        local totalManaBonus = baseManaBonus
        statsTable.bonusMana = totalManaBonus
    end
    return statsTable
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroStrength(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        return hero.stats.str or 0
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroAgility(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        return hero.stats.agi or 0
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroIntellect(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        return hero.stats.int or 0
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroAttackSpeed(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        return hero.stats.attackSpeed or 0
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroSpellDamage(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        return hero.stats.spellDamage or 0
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroSpellHaste(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        return hero.stats.spellHaste or 0
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroAttackRange(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        local baseAttackRange = hero:GetAttackRange()
        local bonusAttackRange = hero.stats.attackRangeBonus or 0
        return baseAttackRange + bonusAttackRange
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroAttackDamage(hero)
    if (hero ~= nil and hero:IsRealHero()) then
        return hero:GetBaseDamageMax()
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroMoveSpeed(hero)
    if (hero ~= nil) then
        return hero:GetMoveSpeedModifier(hero:GetBaseMoveSpeed(), false)
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroManaRegeneration(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        return hero.stats.mpRegen or 0
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroArmor(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        return hero.stats.armor or 0
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroHealthRegeneration(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        return hero.stats.hpRegen or 0
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroBlock(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        return hero.stats.block or 0
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroMagicBlock(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        return hero.stats.magicBlock or 0
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroDamageReduction(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        return hero.stats.damageReduction or 0
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroFireDamage(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        return hero.stats.elementsDamage.fire or 0
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroFrostDamage(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        return hero.stats.elementsDamage.frost or 0
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroEarthDamage(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        return hero.stats.elementsDamage.earth or 0
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroVoidDamage(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        return hero.stats.elementsDamage.void or 0
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroHolyDamage(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        return hero.stats.elementsDamage.holy or 0
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroNatureDamage(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        return hero.stats.elementsDamage.nature or 0
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroInfernoDamage(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        return hero.stats.elementsDamage.inferno or 0
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroFireProtection(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        return hero.stats.elementsProtection.fire or 0
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroFrostProtection(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        return hero.stats.elementsProtection.frost or 0
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroEarthProtection(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        return hero.stats.elementsProtection.earth or 0
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroVoidProtection(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        return hero.stats.elementsProtection.void or 0
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroHolyProtection(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        return hero.stats.elementsProtection.holy or 0
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroNatureProtection(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        return hero.stats.elementsProtection.nature or 0
    end
    return 0
end

---@param hero CDOTA_BaseNPC_Hero
---@return number
function Heroes:GetHeroInfernoProtection(hero)
    if (hero ~= nil and hero:IsRealHero() and hero.stats ~= nil) then
        return hero.stats.elementsProtection.inferno or 0
    end
    return 0
end

modifier_hero_system = modifier_hero_system or class({
    IsDebuff = function(self)
        return false
    end,
    IsHidden = function(self)
        return true
    end,
    IsPurgable = function(self)
        return false
    end,
    RemoveOnDeath = function(self)
        return false
    end,
    AllowIllusionDuplicate = function(self)
        return false
    end,
    GetAttributes = function(self)
        return MODIFIER_ATTRIBUTE_PERMANENT
    end,
})

function modifier_hero_system:DeclareFunctions()
    local funcs = {
        MODIFIER_EVENT_ON_DEATH,
        MODIFIER_EVENT_ON_ORDER,
        MODIFIER_EVENT_ON_ATTACK_LANDED,
        MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
        MODIFIER_PROPERTY_STATS_AGILITY_BONUS,
        MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,
        MODIFIER_PROPERTY_HEALTH_REGEN_CONSTANT,
        MODIFIER_PROPERTY_MANA_REGEN_CONSTANT
    }
    return funcs
end

function modifier_hero_system:GetModifierConstantManaRegen()
    return Heroes:GetHeroManaRegeneration(self.hero)
end

function modifier_hero_system:GetModifierConstantHealthRegen()
    return Heroes:GetHeroHealthRegeneration(self.hero)
end

function modifier_hero_system:GetModifierBonusStats_Strength()
    return -Heroes:GetHeroStrength(self.hero) or 0
end

function modifier_hero_system:GetModifierBonusStats_Agility()
    return -Heroes:GetHeroAgility(self.hero) or 0
end

function modifier_hero_system:GetModifierBonusStats_Intellect()
    return -Heroes:GetHeroIntellect(self.hero) or 0
end

function modifier_hero_system:OnDeath(event)
    local hero = self.hero
    hero.currentLives = hero.currentLives - 1
    if (hero.currentLives <= 0) then
        local modifier = hero:FindModifierByName("modifier_hero_lives")
        if (modifier ~= nil) then
            modifier:Destroy()
        end
        hero:SetTimeUntilRespawn(9999999)
    else
        hero:SetModifierStackCount("modifier_hero_lives", hero, hero.currentLives)
    end
end

function modifier_hero_system:OnAttackLanded(event)
    if IsServer() then
        local attacker = event.attacker
        local target = event.target
        if (attacker ~= nil and target ~= nil and attacker ~= target and attacker == self.hero) then
            modifier_summon:OnSummonMasterAttackLanded(event)
        end
    end
end

function modifier_hero_system:OnOrder(event)
    if IsServer() then
        modifier_summon:OnSummonMasterIssueOrder(event)
    end
end

function modifier_hero_system:OnCreated(event)
    if IsServer() then
        self.hero = self:GetParent()
        self.hero.stats = {
            str = 0,
            agi = 0,
            int = 0,
            damageReduction = 0,
            spellDamage = 0,
            spellHaste = 0,
            armor = 0,
            block = 0,
            magicBlock = 0,
            elementsProtection = {
                fire = 0,
                frost = 0,
                earth = 0,
                void = 0,
                holy = 0,
                nature = 0,
                inferno = 0
            },
            elementsDamage = {
                fire = 0,
                frost = 0,
                earth = 0,
                void = 0,
                holy = 0,
                nature = 0,
                inferno = 0
            }
        }
        self.hero:AddNewModifier(self.hero, nil, "modifier_hero_system_aaspeed", { Duration = -1 })
        self.hero:AddNewModifier(self.hero, nil, "modifier_hero_system_aarange", { Duration = -1 })
        self.hero:AddNewModifier(self.hero, nil, "modifier_hero_system_castrange", { Duration = -1 })
        self.hero:AddNewModifier(self.hero, nil, "modifier_hero_system_movespeed", { Duration = -1 })
        self.hero:AddNewModifier(self.hero, nil, "modifier_hero_system_maxhp", { Duration = -1 })
        self.hero:AddNewModifier(self.hero, nil, "modifier_hero_system_maxmp", { Duration = -1 })
        self:StartIntervalThink(Heroes.STATS_CALCULATE_INTERVAL)
    end
end

function modifier_hero_system:OnIntervalThink()
    if IsServer() then
        local hero = self.hero
        local playerID = hero:GetPlayerID()
        ---@type HERO_STATS_TABLE
        local statsTable = hero.stats
        statsTable = Heroes:CalculateStats(hero, statsTable)
        -- save calculated stats to hero entity
        hero.stats = statsTable
        -- send data to clients
        local dataTable = {
            player_id = playerID,
            statsTable = statsTable
        }
        CustomGameEventManager:Send_ServerToAllClients("rpg_update_hero_stats", { data = json.encode(dataTable) })
    end
end

modifier_hero_lives = modifier_hero_lives or class({
    IsDebuff = function(self)
        return false
    end,
    IsHidden = function(self)
        return false
    end,
    IsPurgable = function(self)
        return false
    end,
    RemoveOnDeath = function(self)
        return false
    end,
    AllowIllusionDuplicate = function(self)
        return false
    end,
    GetTexture = function(self)
        return "pangolier_heartpiercer"
    end,
    GetAttributes = function(self)
        return MODIFIER_ATTRIBUTE_PERMANENT
    end,
})

modifier_hero_system_aaspeed = modifier_hero_system_aaspeed or class({
    IsDebuff = function(self)
        return false
    end,
    IsHidden = function(self)
        return true
    end,
    IsPurgable = function(self)
        return false
    end,
    RemoveOnDeath = function(self)
        return false
    end,
    AllowIllusionDuplicate = function(self)
        return false
    end,
    GetAttributes = function(self)
        return MODIFIER_ATTRIBUTE_PERMANENT
    end,
    DeclareFunctions = function(self)
        return { MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT }
    end,
    GetModifierAttackSpeedBonus_Constant = function(self)
        local hero = self:GetParent()
        local stacks = hero:GetModifierStackCount("modifier_hero_system_aaspeed", hero)
        return stacks
    end
})

modifier_hero_system_aarange = modifier_hero_system_aarange or class({
    IsDebuff = function(self)
        return false
    end,
    IsHidden = function(self)
        return true
    end,
    IsPurgable = function(self)
        return false
    end,
    RemoveOnDeath = function(self)
        return false
    end,
    AllowIllusionDuplicate = function(self)
        return false
    end,
    GetAttributes = function(self)
        return MODIFIER_ATTRIBUTE_PERMANENT
    end,
    DeclareFunctions = function(self)
        return { MODIFIER_PROPERTY_ATTACK_RANGE_BONUS }
    end,
    GetModifierAttackRangeBonus = function(self)
        local hero = self:GetParent()
        local stacks = hero:GetModifierStackCount("modifier_hero_system_aarange", hero)
        return stacks
    end
})

modifier_hero_system_castrange = modifier_hero_system_castrange or class({
    IsDebuff = function(self)
        return false
    end,
    IsHidden = function(self)
        return true
    end,
    IsPurgable = function(self)
        return false
    end,
    RemoveOnDeath = function(self)
        return false
    end,
    AllowIllusionDuplicate = function(self)
        return false
    end,
    GetAttributes = function(self)
        return MODIFIER_ATTRIBUTE_PERMANENT
    end,
    DeclareFunctions = function(self)
        return { MODIFIER_PROPERTY_CAST_RANGE_BONUS }
    end,
    GetModifierCastRangeBonus = function(self)
        local hero = self:GetParent()
        local stacks = hero:GetModifierStackCount("modifier_hero_system_castrange", hero)
        return stacks
    end
})

modifier_hero_system_movespeed = modifier_hero_system_movespeed or class({
    IsDebuff = function(self)
        return false
    end,
    IsHidden = function(self)
        return true
    end,
    IsPurgable = function(self)
        return false
    end,
    RemoveOnDeath = function(self)
        return false
    end,
    AllowIllusionDuplicate = function(self)
        return false
    end,
    GetAttributes = function(self)
        return MODIFIER_ATTRIBUTE_PERMANENT
    end,
    DeclareFunctions = function(self)
        return { MODIFIER_PROPERTY_MOVESPEED_BONUS_CONSTANT }
    end,
    GetModifierMoveSpeedBonus_Constant = function(self)
        local hero = self:GetParent()
        local stacks = hero:GetModifierStackCount("modifier_hero_system_movespeed", hero)
        return stacks
    end
})

modifier_hero_system_maxhp = modifier_hero_system_maxhp or class({
    IsDebuff = function(self)
        return false
    end,
    IsHidden = function(self)
        return true
    end,
    IsPurgable = function(self)
        return false
    end,
    RemoveOnDeath = function(self)
        return false
    end,
    AllowIllusionDuplicate = function(self)
        return false
    end,
    GetAttributes = function(self)
        return MODIFIER_ATTRIBUTE_PERMANENT
    end,
    DeclareFunctions = function(self)
        return { MODIFIER_PROPERTY_HEALTH_BONUS }
    end,
    GetModifierHealthBonus = function(self)
        local hero = self:GetParent()
        return hero.stats.bonusHealth or 0
    end
})

modifier_hero_system_maxmp = modifier_hero_system_maxmp or class({
    IsDebuff = function(self)
        return false
    end,
    IsHidden = function(self)
        return true
    end,
    IsPurgable = function(self)
        return false
    end,
    RemoveOnDeath = function(self)
        return false
    end,
    AllowIllusionDuplicate = function(self)
        return false
    end,
    GetAttributes = function(self)
        return MODIFIER_ATTRIBUTE_PERMANENT
    end,
    DeclareFunctions = function(self)
        return { MODIFIER_PROPERTY_MANA_BONUS }
    end,
    GetModifierManaBonus = function(self)
        local hero = self:GetParent()
        return hero.stats.bonusMana or 0
    end
})

LinkLuaModifier("modifier_hero_lives", "systems/heroes", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_hero_system", "systems/heroes", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_hero_system_aaspeed", "systems/heroes", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_hero_system_aarange", "systems/heroes", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_hero_system_castrange", "systems/heroes", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_hero_system_movespeed", "systems/heroes", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_hero_system_maxhp", "systems/heroes", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_hero_system_maxmp", "systems/heroes", LUA_MODIFIER_MOTION_NONE)
if not Heroes.initialized then
    Heroes:Init()
    Heroes.initialized = true
end