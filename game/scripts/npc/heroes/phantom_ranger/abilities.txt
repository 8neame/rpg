"DOTAAbilities"
{
	//=================================================================================================================
	// Phantom Ranger: Phantom Harmonic 
	//=================================================================================================================
	"phantom_ranger_phantom_harmonic"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_lua"
		"ScriptFile"					"heroes/hero_phantom_ranger"
		"AbilityTextureName"			"phantom_ranger_phantom_harmonic"
        "AbilityType"            "DOTA_ABILITY_TYPE_BASIC"
	    "MaxLevel"               "4"
	    "RequiredLevel"          "0"
	    "LevelsBetweenUpgrades"  "0"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE"

		// Time		
		//-------------------------------------------------------------------------------------------------------------
		//"AbilityCooldown"				"0"

		// Cost
		//-------------------------------------------------------------------------------------------------------------
		//"AbilityManaCost"				"0"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"max_stacks"				"15"
			}
			"02"
			{
				"var_type"				"FIELD_INTEGER"
				"proc_chance"				"15 20 25 30"
			}
			"03"
			{
				"var_type"				"FIELD_FLOAT"
				"proc_damage"				"15 20 25 30"
			}
			"04"
			{
				"var_type"				"FIELD_FLOAT"
				"cdr_per_stack"				"0.1 0.15 0.2 0.25"
			}
			"05"
			{
				"var_type"				"FIELD_FLOAT"
				"duration"				"5"
			}
		}
		"precache"
		{
			"particle" "particles/units/phantom_ranger/phantom_ranger_phantom_harmonic_proj.vpcf"
		}
	}
	//=================================================================================================================
	// Phantom Ranger: Void Disciple
	//=================================================================================================================
	"phantom_ranger_void_disciple"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_lua"
		"ScriptFile"					"heroes/hero_phantom_ranger"
		"AbilityTextureName"			"phantom_ranger_void_disciple"
        "AbilityType"            "DOTA_ABILITY_TYPE_BASIC"
	    "MaxLevel"               "4"
	    "RequiredLevel"          "0"
	    "LevelsBetweenUpgrades"  "0"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_NO_TARGET | DOTA_ABILITY_BEHAVIOR_IGNORE_BACKSWING"
	
		// Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastAnimation"			"ACT_DOTA_CAST_ABILITY_3"
		"AbilityCastGestureSlot"		"DEFAULT"
		"AbilityCastPoint"				"0.25"

		// Time		
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"10 11 12 13"

		// Cost
		//-------------------------------------------------------------------------------------------------------------
		"AbilityManaCost"				"10 20 30 40"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"max_voids"				"2 3 4 5"
			}
			"02"
			{
				"var_type"				"FIELD_INTEGER"
				"duration"				"5 6 7 8"
			}
			"03"
			{
				"var_type"				"FIELD_INTEGER"
				"void_damage"				"20 30 40 50"
			}
			"04"
			{
				"var_type"				"FIELD_INTEGER"
				"void_aa_speed"				"100"
			}
		}
		"precache"
		{
			"particle" "particles/units/phantom_ranger/test/void_disciple/void_disciple_effect.vpcf"
		    "soundfile" "soundevents/game_sounds_heroes/game_sounds_void_spirit.vsndevts"
		}
	}
	//=================================================================================================================
	// Phantom Ranger: Shadow Waves
	//=================================================================================================================
	"phantom_ranger_shadow_waves"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_lua"
		"ScriptFile"					"heroes/hero_phantom_ranger"
		"AbilityTextureName"			"phantom_ranger_shadow_waves"
		
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_POINT"
        "AbilityType"            "DOTA_ABILITY_TYPE_BASIC"
	    "MaxLevel"               "4"
	    "RequiredLevel"          "0"
	    "LevelsBetweenUpgrades"  "0"
		"SpellDispellableType"			"SPELL_DISPELLABLE_YES"
	
		// Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastRange"				"1000"
		"AbilityCastPoint"				"0.25"
		"AbilityCastAnimation"		"ACT_DOTA_CAST_ABILITY_2"

		// Time		
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"20"

		// Cost
		//-------------------------------------------------------------------------------------------------------------
		"AbilityManaCost"				"25 50 75 100"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"				"FIELD_FLOAT"
				"ms_slow"				"-10 -15 -20 -25"
			}
			"02"
			{
				"var_type"				"FIELD_FLOAT"
				"as_slow"				"-6 -9 -12 -15"
			}
			"03"
			{
				"var_type"				"FIELD_FLOAT"
				"cast_speed"				"-6 -9 -12 -15"
			}
			"04"
			{
				"var_type"				"FIELD_FLOAT"
				"duration"				"3"
			}
			"05"
			{
				"var_type"				"FIELD_FLOAT"
				"silence_duration"				"0.2"
			}
			"precache"
			{
				"particle" "particles/units/phantom_ranger/phantom_ranger_shadow_wave_proj.vpcf"
			}
		}
	}
	//=================================================================================================================
	// Phantom Ranger: Soul Echo
	//=================================================================================================================
	"phantom_ranger_soul_echo"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_lua"
		"ScriptFile"					"heroes/hero_phantom_ranger"
		"AbilityTextureName"			"phantom_ranger_soul_echo"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_NO_TARGET | DOTA_ABILITY_BEHAVIOR_IMMEDIATE"
		"SpellDispellableType"			"SPELL_DISPELLABLE_NO"
        "AbilityType"            "DOTA_ABILITY_TYPE_BASIC"
	    "MaxLevel"               "4"
	    "RequiredLevel"          "0"
	    "LevelsBetweenUpgrades"  "0"

		// Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastAnimation"			"ACT_DOTA_CAST_ABILITY_2"
		"AbilityCastRange"				"0"
		"AbilityCastPoint"				"0.0 0.0 0.0 0.0"

		// Time		
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"30 28 26 24"

		// Cost
		//-------------------------------------------------------------------------------------------------------------
		"AbilityManaCost"				"125"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"				"FIELD_FLOAT"
				"damage_reduction"				"20 30 40 50"
			}
			"02"
			{
				"var_type"				"FIELD_FLOAT"
				"duration"				"5"
			}
			"03"
			{
				"var_type"				"FIELD_FLOAT"
				"phantom_health"				"70 100 130 160"
			}
		}
		"precache"
		{
		    "soundfile" "soundevents/game_sounds_heroes/game_sounds_void_spirit.vsndevts"
			"particle" "particles/units/phantom_ranger/test/soul_echo/soul_echo_phantom.vpcf"
			"particle" "particles/status_fx/status_effect_maledict.vpcf"
		}
	}
}