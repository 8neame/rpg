var localPlayerId;
var bossPanel, bossImage, bossName, bossHpBar, bossHpBarValue, bossHpBarRegValue, bossMpBar, bossMpBarValue, bossMpBarRegValue, bossAbilitiesPanel;
var bossAbilties = [];
var MAX_ABILITIES_ON_PANEL = 10;
var latestSelectedCreep;

function OnPortraitClick() {
	var IsAltDown = GameUI.IsAltDown();
	GameUI.SetCameraTargetPosition(Entities.GetAbsOrigin(latestSelectedCreep), -1.0);
	if(IsAltDown) {
		var player = Players.GetLocalPlayer();
		var message = "Enemy %unitName% is still alive.";
		//var message = $.Localize("#DOTA_Tooltip_enemy_is_alive");
		message = message.replace("%unitName%", $.Localize("#" + Entities.GetUnitName(latestSelectedCreep)));
		GameEvents.SendCustomGameEventToServer("rpg_say_chat_message", { "player_id" : player, "msg" : message});
	}
}

function OnHPBarClick() {
	var IsAltDown = GameUI.IsAltDown();
	if(IsAltDown) {
		var player = Players.GetLocalPlayer();
		var currentHp = Entities.GetHealth(latestSelectedCreep);
		var maxHp = Entities.GetMaxHealth(latestSelectedCreep);
		var hpPercent = (currentHp / maxHp) * 100;
		hpPercent = Math.round(hpPercent * 100) / 100;
		var message = "Enemy " + $.Localize("#" + Entities.GetUnitName(latestSelectedCreep)) + " have " + currentHp + " (" + hpPercent + "%) health left.";
		GameEvents.SendCustomGameEventToServer("rpg_say_chat_message", { "player_id" : player, "msg" : message});
	}
}

function OnMPBarClick() {
	var IsAltDown = GameUI.IsAltDown();
	if(IsAltDown) {
		var player = Players.GetLocalPlayer();
		var currentMp = Entities.GetMana(latestSelectedCreep);
		var maxMp = Entities.GetMaxMana(latestSelectedCreep);
		var mpPercent = (currentMp / maxMp) * 100;
		mpPercent = Math.round(mpPercent * 100) / 100;
		if (!isNaN(mpPercent)) {
			var message = "Enemy " + $.Localize("#" + Entities.GetUnitName(latestSelectedCreep)) + " have " + currentMp + " (" + mpPercent + "%) mana left.";
			GameEvents.SendCustomGameEventToServer("rpg_say_chat_message", { "player_id" : player, "msg" : message});
		}
	}
}

function UpdateSelection() {
    var selectedCreep = Players.GetLocalPlayerPortraitUnit();
    if (Entities.GetTeamNumber(selectedCreep) != Players.GetTeam(localPlayerId)) {
        bossPanel.style.visibility = "visible";
        latestSelectedCreep = selectedCreep;
        var currentAbilities = 0;
        var abiltiesCount = Entities.GetAbilityCount(selectedCreep);
        for (var i = 0; i < MAX_ABILITIES_ON_PANEL; i++) {
            bossAbilties[i].Data().abilityPanel.abilityname = "empty";
            bossAbilties[i].Data().cooldownPanel.SetHasClass("in_cooldown", false);
            bossAbilties[i].Data().ready = false;
        }
		bossImage.SetUnit(Entities.GetUnitName(selectedCreep), "1", false);
        var lastLegitIndex = 0;
        for (var i = 0; i < abiltiesCount; i++) {
            var ability = Entities.GetAbility(selectedCreep, i);
            var IsValidAbility = (Abilities.GetLevel(ability) > 0 && Abilities.GetMaxLevel(ability) > 0 && Abilities.GetManaCost(ability) > -1 && !Abilities.IsHidden(ability));
            if (IsValidAbility) {
                bossAbilties[lastLegitIndex].Data().abilityPanel.abilityname = Abilities.GetAbilityName(ability);
                bossAbilties[lastLegitIndex].Data().abilityIndex = i;
                bossAbilties[lastLegitIndex].Data().ability = ability;
                bossAbilties[lastLegitIndex].Data().ready = true;
                lastLegitIndex++;
                currentAbilities++;
            }
            if (currentAbilities >= MAX_ABILITIES_ON_PANEL) {
                break;
            }
        }
    }
}

function UpdateValues() {
    if (latestSelectedCreep != null && Entities.GetTeamNumber(latestSelectedCreep) != Players.GetTeam(localPlayerId)) {
        bossPanel.style.visibility = Entities.IsAlive(latestSelectedCreep) ? "visible" : "collapse";
    } else {
        return;
    }
    var name = $.Localize("#" + Entities.GetUnitName(latestSelectedCreep));
    var currentHp = Entities.GetHealth(latestSelectedCreep);
    var maxHp = Entities.GetMaxHealth(latestSelectedCreep);
    var hpReg = Entities.GetHealthThinkRegen(latestSelectedCreep);
    var hpPercent = (currentHp / maxHp) * 100;
    var currentMp = Entities.GetMana(latestSelectedCreep);
    var maxMp = Entities.GetMaxMana(latestSelectedCreep);
    var mpReg = Entities.GetManaThinkRegen(latestSelectedCreep);
    var mpPercent = (currentMp / maxMp) * 100;
	var IsAltDown = GameUI.IsAltDown();
    if (isNaN(mpPercent)) {
        mpPercent = 100;
    }
    bossName.text = name;
	if(!IsAltDown) {
		bossHpBarValue.text = currentHp + " / " + maxHp;
	} else {
		bossHpBarValue.text = (Math.round(hpPercent * 100) / 100) + "%";
	}
    bossHpBar.style.width = hpPercent + "%";
    bossHpBarRegValue.text = "+" + (Math.round(hpReg * 100) / 100);
    bossMpBar.style.width = mpPercent + "%";
    if (maxMp == 0) {
        bossMpBarValue.text = "";
        bossMpBarRegValue.text = "";
    } else {
		if(!IsAltDown) {
			bossMpBarValue.text = currentMp + " / " + maxMp;
		} else {
			bossMpBarValue.text = (Math.round(mpPercent * 100) / 100) + "%";
		}
		bossMpBarRegValue.text = "+" + (Math.round(mpReg * 100) / 100);
    }
    for (var i = 0; i < MAX_ABILITIES_ON_PANEL; i++) {
        if (bossAbilties[i].Data().abilityPanel.abilityname !== "empty" && bossAbilties[i].Data().ready) {
            var ability = bossAbilties[i].Data().ability;
            var IsAbilityReady = Abilities.IsCooldownReady(ability);
            var manacost = Abilities.GetManaCost(ability);
            bossAbilties[i].Data().cooldownPanel.SetHasClass("in_cooldown", !IsAbilityReady);
            if (!IsAbilityReady) {
                var cooldownLength = Abilities.GetCooldownLength(ability);
                var cooldownRemaining = Abilities.GetCooldownTimeRemaining(ability);
                var cooldownPercent = Math.ceil(100 * (cooldownRemaining / cooldownLength));
                var cooldownOverlayValue = cooldownPercent * 3.6;
                bossAbilties[i].Data().cooldownOverlay.style.clip = "radial(50% 50%, 0deg, " + -cooldownOverlayValue + "deg)";
                bossAbilties[i].Data().cooldownTimer.text = Math.ceil(cooldownRemaining);
            }
            bossAbilties[i].Data().manacost.text = manacost > 0 ? manacost : "";
            bossAbilties[i].Data().abilityPanel.SetHasClass("insufficient_mana", manacost > currentMp);
        }
    }
}

function AutoUpdateValues() {
    UpdateValues();
    $.Schedule(0.1, function() {
        AutoUpdateValues();
    });
}

(function() {
    var root = $.GetContextPanel();
    bossPanel = root.GetChild(0);
    bossImage = $('#BossPortraitImage');
    bossName = $('#BossName');
    bossHpBar = $('#BossHpBar');
    bossHpBarValue = $('#BossHpBarValue');
    bossHpBarRegValue = $('#BossHpBarRegValue');
    bossMpBar = $('#BossMpBar');
    bossMpBarValue = $('#BossMpBarValue');
    bossMpBarRegValue = $('#BossMpBarRegValue');
    bossAbilitiesPanel = $('#BossAbilitiesPanel');
    localPlayerId = Players.GetLocalPlayer();
    for (var i = 0; i < MAX_ABILITIES_ON_PANEL; i++) {
        var abilityPanel = $.CreatePanel("Panel", bossAbilitiesPanel, "");
        abilityPanel.BLoadLayout("file://{resources}/layout/custom_game/boss_hpbar_ability.xml", false, false);
        abilityPanel.Data().abilityPanel = abilityPanel.GetChild(0);
        abilityPanel.Data().cooldownPanel = abilityPanel.GetChild(1);
        abilityPanel.Data().cooldownOverlay = abilityPanel.Data().cooldownPanel.GetChild(0);
        abilityPanel.Data().cooldownTimer = abilityPanel.Data().cooldownPanel.GetChild(1);
        abilityPanel.Data().manacost = abilityPanel.GetChild(2);
        abilityPanel.Data().ready = false;
        bossAbilties.push(abilityPanel);
    }
    GameEvents.Subscribe("dota_player_update_query_unit", UpdateSelection);
    GameEvents.Subscribe("dota_player_update_selected_unit", UpdateSelection);
    AutoUpdateValues();
})();